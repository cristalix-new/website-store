<?php
//Cloudflare returns users' ip in other field
if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
  $_SERVER['CF_IP'] = $_SERVER['REMOTE_ADDR'];
  $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}
error_reporting(E_ERROR | E_WARNING | E_PARSE);

//Now, let's start
require_once('../start.php');
?>