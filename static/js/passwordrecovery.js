$(document).ready(function(){
	$('#forgotpass-form').on('submit', function(){
		var form = $(this);
		$.post('/data/account/forgotpassword/require', form.serializeArray())
		.done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			if(data.status == 'ok'){
				let textContainer = form.parent();
				textContainer.html(data.text);
			} else {
				if(data.status == 'redirect'){
					$.post(data.url, {username_email:form.find('[name="data"]').val()})
					.done(function(){
						let textContainer = form.parent();
						textContainer.html('Проверьте свой почтовый ящик и перейдите по ссылке из письма.');
					});
				} else {
					eModal.alert({
						title: 'Ошибка смены пароля',
						message: data.error_rus
					});
				}
			}
		});
		return false;
	});

	var timer;
	function checkPassword(input) {
		let re = new RegExp(input.dataset.regex);
		if(re.test(input.value)){
			$('#input-password').addClass('is-valid').removeClass('is-invalid');
			$('#password-help').removeClass('text-danger').addClass('text-success').text(input.dataset.goodHint);
		} else {
			$('#input-password').addClass('is-invalid').removeClass('is-valid');
			$('#password-help').removeClass('text-success').addClass('text-danger').text(input.dataset.hint);
		}
		countErrors();
	}

	function checkAgainPassword() {
		if($('#input-again-password').val() == $('#input-password').val()){
			$('#input-again-password').addClass('is-valid').removeClass('is-invalid');
			$('#again-password-help').removeClass('text-danger').addClass('text-success').text($('#input-again-password').data('good-hint'));
		} else {
			$('#input-again-password').addClass('is-invalid').removeClass('is-valid');
			$('#again-password-help').removeClass('text-success').addClass('text-danger').text($('#input-again-password').data('hint'));
		}
		countErrors();
	}

	function countErrors(){
		let emptyInputs = 0;
		$('#input-login, #input-password, #input-again-password, #input-email')
			.each(function(k, v){
				if ($(v).val() == ''){
					emptyInputs++;
				}
			})
		if($('.is-invalid').length + emptyInputs > 0){
			$('#submit-register').prop('disabled', true).addClass('disable-cur');
		} else {
			$('#submit-register').prop('disabled', false).removeClass('disable-cur');
		}
	}

	$('#input-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){
			checkPassword(input);
			if($('#input-again-password').val() != ''){
				checkAgainPassword();
			}
		}, 1000);
	});

	$('#input-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkPassword(input);
		if($('#input-again-password').val() != ''){
			checkAgainPassword();
		}
	});


	$('#input-again-password').on('keyup', function(){
		var input = this;
		clearTimeout(timer);
		timer = setTimeout(function(){checkAgainPassword(input)}, 100);
	});

	$('#input-again-password').on('focusout', function(){
		var input = this;
		clearTimeout(timer);
		checkAgainPassword();
	});

	$('#chpass-form').on('submit', function(){
		let form = $(this);
		$.post('/data/account/forgotpassword/change', form.serializeArray()).
		done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			if(data.status == 'ok'){
				let textContainer = form.parent();
				textContainer.html('<strong>Пароль успешно изменён.</strong><br>Вы можете с ним <a href="#" class="auth-dropdown-opener">войти</a>.');
			} else {
				eModal.alert({
					title: 'Ошибка смены пароля',
					message: data.error_rus + "<br> Возможно, код устарел. Попробуйте <a href='/account/forgotpassword/'>ещё раз</a>."
				});
			}
		});
		return false;
	})

	countErrors();
});