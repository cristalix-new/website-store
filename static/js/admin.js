$(document).ready(function(){
	$('#updEmail').on('click', function(e){
		let el = $(e.target);
		let dataset = {
			'email' : el.data('email'),
			[el.data('changetype')] : el.data('findme')
		}
		$.post('/data/admin/change_email', dataset)
		.done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			if(data.status == 'ok'){
				eModal.alert({
					title: 'Успех',
					message: 'Почта изменена на ' + data.email + '. Обновите страницу для просмотра изменений.'
				});
				$('#changePass').modal('hide');
			} else {
				eModal.alert({
					title: 'Ошибка',
					message: data.error_rus
				});
			}
		});
	});
	$('#saveGroup').on('click', function(e){
		let dataset = {
			'group' : $('#groupSelect>option:selected').data('group-id'),
			'id' : $('#userId').data('user-id')
		}
		$.post('/data/admin/change_group', dataset)
		.done(function(data){
			if (typeof(data) == 'string') {
				data = JSON.parse(data);
			}
			if(data.status == 'ok'){
				eModal.alert({
					title: 'Успех',
					message: 'Группа изменена на ' + $('#groupSelect').val()
				});
			} else {
				eModal.alert({
					title: 'Ошибка',
					message: data.error_rus
				});
			}
		});
	});
});