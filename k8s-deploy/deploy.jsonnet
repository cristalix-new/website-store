local is_production = std.extVar('IS_PRODUCTION') == 'true';
local namespace = std.extVar('KUBE_NAMESPACE');
local base_domain = std.extVar('BASE_DOMAIN');

[
  {
    kind: 'Secret',
    apiVersion: 'v1',
    metadata: {
      name: 'pull-credentials',
    },
    data: {
      '.dockerconfigjson': std.base64(std.manifestJson({
        auths: {
          [std.extVar('CI_REGISTRY')]: {
            auth: std.base64(std.extVar('CI_DEPLOY_USER') + ':' + std.extVar('CI_DEPLOY_PASSWORD'))
          },
        },
      })),
    },
    type: 'kubernetes.io/dockerconfigjson',
  },

  {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: 'website-store',
      labels: {
        app: 'website-store'
      },
      annotations: {
        'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
        'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: 'website-store',
        },
      },
      template: {
        metadata: {
          labels: {
            app: 'website-store',
          },
          annotations: {
            'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
            'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
          }
        },
        spec: {
          containers: [
            {
              image: '%s:%s' % [std.extVar('CI_REGISTRY_IMAGE'), std.extVar('CI_COMMIT_SHA')],
              name: 'website-store',
              env: [
                {
                  name: 'WEBSITE_ENDPOINT',
                  value: 'http://website.website/api/store/callback'
                }
              ],
              ports: [
                {
                  containerPort: 8080,
                  protocol: 'TCP',
                },
              ],
            },
          ],
          imagePullSecrets: [
            {
              name: 'pull-credentials'
            },
          ],
        },
      },
    },
  },

  {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: 'website-store',
    },
    spec: {
      selector: {
        app: 'website-store',
      },
      ports: [
        {
          name: 'http',
          protocol: 'TCP',
          port: 80,
          targetPort: 8080,
        }
      ],
    },
  },

  {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: 'website-store',
    },
    spec: {
      rules: [
        {
          host: base_domain,
          http: {
            paths: [
              {
                path: '/',
                pathType: 'Prefix',
                backend: {
                  service: {
                    name: 'website-store',
                    port: {
                      number: 80
                    },
                  },
                },
              },
            ],
          },
        },
      ],
    },
  },
]