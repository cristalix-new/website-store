import Bluebird from "bluebird";
// eslint-disable-next-line @typescript-eslint/no-explicit-any
global.Promise = <any>Bluebird;

import dotenv from "dotenv";

dotenv.config();

import express from "express";
import bodyParser from "body-parser";
import axios from "axios";
import crypto from "crypto";

import ejs from "ejs";

const CALLBACK_URL = process.env.CALLBACK_URL ?? "http://localhost:8081/api/store/callback";
const SOURCE_SECRET = process.env.SOURCE_SECRET ?? "source-secret";
const SUCCESS_REDIRECT_URL = process.env.SUCCESS_REDIRECT_URL ?? "http://localhost:8081/payment/success";
const FAILED_REDIRECT_URL = process.env.FAILED_REDIRECT_URL ?? "http://localhost:8081/payment/failed";

const server = express();

server.set("x-powered-by", false);
server.set("etag", false);
server.use(bodyParser.urlencoded({
    extended: true
}));

function checkSignature(url: string, secret: string): boolean {
    return secret === crypto.createHash("sha256").update(secret)
        .update(url).update(secret).digest("hex");
}

// statics

server.get("/", async (req, res) => {
    res.status(200).type("html").end(await ejs.renderFile("templates/index.ejs", {}, {
        root: "templates"
    }), "utf8");
});

server.get("/info/poth", async (req, res) => {
    res.status(200).type("html").end(await ejs.renderFile("templates/poth.ejs", {}, {
        root: "templates"
    }), "utf8");
});

server.get("/info/prison", async (req, res) => {
    res.status(200).type("html").end(await ejs.renderFile("templates/prison.ejs", {}, {
        root: "templates"
    }), "utf8");
});

// redirect to payment system

server.get("/pay", (req, res) => {
    const url = req.query["url"];
    const sign = req.query["sign"];
    if (!url || !sign) {
        res.status(302).redirect("/");
        return;
    }
    const trueUrl = url.toString();
    if (!checkSignature(trueUrl, SOURCE_SECRET)) {
        res.status(302).redirect("/");
        return;
    }
    res.status(200).end(
        `<html lang="ru"><head><title>cristalix.store</title></head><body><script>window.location=${
            JSON.stringify(trueUrl)}</script></body></html>`);
});

// redirects back to cristalix.ru

server.get("/success", async (req, res) => {
    res.status(302).redirect(SUCCESS_REDIRECT_URL);
});

server.get("/failure", async (req, res) => {
    res.status(302).redirect(FAILED_REDIRECT_URL);
});

// callback

server.post("/callback", async (req, res) => {
    const type = req.query["type"];
    if (!type) {
        res.status(400).json({
            error: "no type present in query"
        });
        return;
    }
    const trueType = type.toString();
    try {
        res.status(200).json((await axios.post(CALLBACK_URL, {
            type: trueType,
            data: JSON.stringify(req.body)
        })).data);
    } catch (e) {
        res.status(500).json({
            error: e
        });
    }
});

// static

server.use("/", express.static("static"));

// 404

server.use(async (req, res) => {
    res.status(404).type("html").end(await ejs.renderFile("templates/404.ejs", {}, {
        root: "templates"
    }), "utf8");
});

server.listen(parseInt(<string>process.env.SERVER_PORT) || 8080);